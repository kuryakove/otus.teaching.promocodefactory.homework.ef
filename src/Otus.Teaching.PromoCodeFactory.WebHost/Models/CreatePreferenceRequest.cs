﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreatePreferenceRequest
    {
        public string Name { get; set; }
    }
}
