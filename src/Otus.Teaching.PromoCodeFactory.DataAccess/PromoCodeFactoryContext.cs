﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Many_to_many;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Constants;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PromoCodeFactoryContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; } = null!;
        public DbSet<Employee> Employees { get; set; } = null!;
        public DbSet<Role> Roles { get; set; } = null!;
        public DbSet<Preference> Preferences { get; set; } = null!;
        public DbSet<PromoCode> PromoCodes { get; set; } = null!;
        public DbSet<CustomerPreference> CustomerPreferences { get; set; } = null!;

        public PromoCodeFactoryContext(DbContextOptions<PromoCodeFactoryContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
            modelBuilder
                .Entity<Preference>()
                .HasMany(c => c.Customers)
                .WithMany(p => p.Preferences)
                .UsingEntity<CustomerPreference>(
                    j => j
                        .HasOne(customerPreference => customerPreference.Customer)
                        .WithMany(customer => customer.CustomerPreferences)
                        .HasForeignKey(customerPreference => customerPreference.CustomerId),
                    j => j
                        .HasOne(customerPreference => customerPreference.Preference)
                        .WithMany(preference => preference.CustomerPreferences)
                        .HasForeignKey(customerPreference => customerPreference.PreferenceId));

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(string)))
            {
                if (property.GetMaxLength() == null)
                    property.SetMaxLength(DataBaseConfigurationConstants.StringMaxLength);
            }

            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
        }
    }
}
