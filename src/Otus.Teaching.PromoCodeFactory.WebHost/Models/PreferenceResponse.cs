﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public string Name { get; set; }

        public PromoCodeShortResponse PromoCode { get; set; }
        public IEnumerable<CustomerShortResponse> Customers { get; set; }
    }
}
