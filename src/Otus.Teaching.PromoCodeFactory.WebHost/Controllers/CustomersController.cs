﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Many_to_many;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        /// <summary>
        /// Initialize client controller with specified dependencies
        /// </summary>
        /// <param name="customerRepository">Repository to work with customers</param>
        /// <param name="preferenceRepository">Repository to work with preferences</param>
        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns>Collection of all customers</returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync(); 

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            });

            return Ok(response);
        }

        /// <summary>
        /// Get specific customer using id
        /// </summary>
        /// <param name="id">Id of the customer that should be returned</param>
        /// <returns>Customer founded in db or null if nothing was found</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerResponse = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes
                    .Select(promo => new PromoCodeShortResponse()
                    {
                        Code = promo.Code,
                        BeginDate = promo.BeginDateTime.ToShortDateString(),
                        EndDate = promo.EndDateTime.ToShortDateString(),
                        PartnerName = promo.PartnerName,
                        ServiceInfo = promo.ServiceInfo,
                        Id = promo.Id,
                    })
                    .ToList(),
                Preferences = customer.Preferences
                    .Select(preference => new PreferenceShortResponse() {Name = preference.Name})
                    .ToList()
            };

            return Ok(customerResponse);
        }

        /// <summary>
        /// Create customer with specified in body json model
        /// </summary>
        /// <param name="request">Customer DTO that should be created</param>
        /// <returns>Ok if customer was created successfully, 400 if param was null</returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }
            
            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = new List<Preference>(),
                CustomerPreferences = new List<CustomerPreference>()
            };

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds ?? Enumerable.Empty<Guid>());
            
            customer.Preferences.ToList().AddRange(preferences);
            foreach (var preference in preferences)
                preference.Customers.Add(customer);
            
            await _customerRepository.AddAsync(customer);
            await _preferenceRepository.UpdateAsync();

            return Ok();
        }

        /// <summary>
        /// Edit existed customer
        /// </summary>
        /// <param name="id">Id of customer to edit</param>
        /// <param name="request">New model that should be saved</param>
        /// <returns>Ok if changes applied successfully, 400 if param was null, 404 if customer wasn't found</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            customer.FirstName = request.FirstName ?? customer.FirstName;
            customer.LastName = request.LastName ?? customer.LastName;
            customer.Email = request.Email ?? customer.Email;

            var newPreferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            var customerPreferences = await _preferenceRepository.GetRangeByIdsAsync(customer.Preferences?.Select(p => p.Id));

            foreach (var preference in customerPreferences) preference.Customers.Remove(customer);
            foreach (var preference in newPreferences) preference.Customers.Add(customer);
            customer.Preferences = (ICollection<Preference>)newPreferences;

            await _customerRepository.UpdateAsync();
            await _preferenceRepository.UpdateAsync();

            return Ok();
        }

        /// <summary>
        /// Delete customer with specific id
        /// </summary>
        /// <param name="id">Id of customer to delete</param>
        /// <returns>Ok if customer deleted successfully, 404 if customer wasn't found</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return  Ok();
        }
    }
}