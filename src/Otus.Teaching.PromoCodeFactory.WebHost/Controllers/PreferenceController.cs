﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Controller for preferences
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Get all preferences
        /// </summary>
        /// <returns>Collection of all preferences</returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceShortResponse>>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var response = preferences.Select(p => new PreferenceShortResponse()
            {
                Name = p.Name
            });

            return Ok(response);
        }

        /// <summary>
        /// Get specific preference using id
        /// </summary>
        /// <param name="id">Id of the preference that should be returned</param>
        /// <returns>Preference founded in db or null if nothing was found</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PreferenceResponse>> GePreferenceAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
            {
                return NotFound();
            }

            var response = new PreferenceResponse()
            {
                Name = preference.Name,
                Customers = preference.Customers
                    .Select(c => new CustomerShortResponse()
                    {
                        FirstName = c.FirstName,
                        Email = c.Email,
                        Id = c.Id,
                        LastName = c.LastName
                    })
            };

            if (preference.PromoCode != null)
            {
                response.PromoCode = new PromoCodeShortResponse()
                {
                    Code = preference.PromoCode.Code,
                    BeginDate = preference.PromoCode.BeginDateTime.ToShortDateString(),
                    EndDate = preference.PromoCode.EndDateTime.ToShortDateString(),
                    PartnerName = preference.PromoCode.PartnerName,
                    ServiceInfo = preference.PromoCode.ServiceInfo,
                    Id = preference.PromoCode.Id,
                };
            }

            return Ok(response);
        }

        /// <summary>
        /// Create preference with specified in body json model
        /// </summary>
        /// <param name="request">Preference DTO that should be created</param>
        /// <returns>Ok if preference was created successfully, 400 if param was null</returns>
        [HttpPost]
        public async Task<IActionResult> CreatePreferenceAsync(CreatePreferenceRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            var preference = new Preference()
            {
                Id = Guid.NewGuid(),
                Name = request.Name
            };

            await _preferenceRepository.AddAsync(preference);

            return Ok();
        }

        /// <summary>
        /// Delete preference with specific id
        /// </summary>
        /// <param name="id">Id of preference to delete</param>
        /// <returns>Ok if preference deleted successfully, 404 if preference wasn't found</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePreference(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            await _preferenceRepository.DeleteAsync(preference);

            return Ok();
        }
    }
}
