﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Many_to_many;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }

        public virtual PromoCode PromoCode { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
    }
}