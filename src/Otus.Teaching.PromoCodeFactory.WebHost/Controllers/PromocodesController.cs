﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<Customer> customerRepository,
                                    IRepository<Employee> employeeRepository)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns>Все промокоды</returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promocodeRepository.GetAllAsync();

            var respones = promoCodes.Select(p => new PromoCodeShortResponse()
            {
                BeginDate = p.BeginDateTime.ToShortDateString(),
                Code = p.Code,
                EndDate = p.EndDateTime.ToShortDateString(),
                Id = p.Id,
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            });

            return Ok(respones);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns>Ок, если промокод создан успешно</returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customer = await _customerRepository.GetFirstWhere(c => c.Preferences.Any(p => p.Name == request.Preference));
            var employee = await _employeeRepository.GetFirstWhere(e => e.FirstName == request.PartnerName);
            var preference = await _preferenceRepository.GetFirstWhere(p => p.Name == request.Preference);

            var promocode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                BeginDateTime = DateTime.Now,
                EndDateTime = DateTime.Now.AddMonths(1),
                CustomerId = customer.Id,
                PartnerManagerId = employee.Id,
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                PreferenceId = preference.Id
            };

            await _promocodeRepository.AddAsync(promocode);

            return Ok();
        }
    }
}