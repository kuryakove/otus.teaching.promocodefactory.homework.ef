﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Migrations
{
    public partial class Renamefieldsinpromocode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EndDate",
                table: "PromoCodes",
                newName: "EndDateTime");

            migrationBuilder.RenameColumn(
                name: "BeginDate",
                table: "PromoCodes",
                newName: "BeginDateTime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EndDateTime",
                table: "PromoCodes",
                newName: "EndDate");

            migrationBuilder.RenameColumn(
                name: "BeginDateTime",
                table: "PromoCodes",
                newName: "BeginDate");
        }
    }
}
